#include <stdio.h>
#include <stdlib.h>

int main()
{
    int x,y;
    printf("ENTER TWO NUMBERS X AND Y\n");
    scanf("%d%d",&x,&y);
    printf("BEFORE SWAPPING\n first integer = %d\n second integer = %d\n",x,y);
    void swap(int*,int*);//swap function declaration
    swap (&x,&y);
    printf("AFTER SWAPPING\n first integer = %d\n second integer = %d\n",x,y);
    return 0;
}
//swap function definition
void swap(int*a,int*b)
{
    //declaring third variable
    int c;
    c=*b;
    *b=*a;
    *a=c;
}
