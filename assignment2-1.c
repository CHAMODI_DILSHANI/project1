#include <stdio.h>
#include <stdlib.h>

int main()
{
   int num;
   printf("ENTER ANY NUMBER:");
   scanf("%d", &num);
   if(num > 0)
   {
       printf("NUMBER IS POSITIVE");
   }
   else if (num < 0)
   {
       printf("NUMBER IS NEGATIVE");

   }
   else
   {
       printf("NUMBER IS ZERO");
   }

    return 0;
}
